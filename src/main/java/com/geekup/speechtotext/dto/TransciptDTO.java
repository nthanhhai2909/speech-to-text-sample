package com.geekup.speechtotext.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class TransciptDTO implements Serializable {

    private static final long serialVersionUID = -1714933686124204464L;

    private String word;

    private String vnTranslateText;

    private Long startTime;

    private Long endTime;

}
