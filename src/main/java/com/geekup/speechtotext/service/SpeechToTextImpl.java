package com.geekup.speechtotext.service;

import com.geekup.speechtotext.config.GoogleCloudEnv;
import com.geekup.speechtotext.dto.TransciptDTO;
import com.google.api.gax.longrunning.OperationFuture;
import com.google.cloud.speech.v1.*;
import com.google.cloud.speech.v1.RecognitionConfig.AudioEncoding;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.translate.v3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


@Service
public class SpeechToTextImpl implements SpeechToTextService {

    @Autowired
    private SpeechClient speechClient;

    @Autowired
    private Storage storage;

    @Autowired
    private TranslationServiceClient translationServiceClient;

    @Autowired
    private GoogleCloudEnv googleCloudEnv;

    @Override
    public List<TransciptDTO> transcript(MultipartFile multipartFile) throws IOException, ExecutionException, InterruptedException {
        String uri = upload(multipartFile);
        RecognitionConfig config =
                RecognitionConfig.newBuilder()
                        .setEncoding(AudioEncoding.FLAC)
                        .setSampleRateHertz(44100)
                        .setLanguageCode("en-US")
                        .setAudioChannelCount(2)
                        .setEnableSeparateRecognitionPerChannel(true)
                        .setEnableWordTimeOffsets(true)
                        .build();

        RecognitionAudio audio = RecognitionAudio.newBuilder().setUri(uri).build();

        LongRunningRecognizeRequest request =
                LongRunningRecognizeRequest.newBuilder().setConfig(config).setAudio(audio).build();

        OperationFuture<LongRunningRecognizeResponse, LongRunningRecognizeMetadata> response = speechClient.longRunningRecognizeAsync(request);

        while (!response.isDone()) {
            System.out.println("Waiting for response...");
            Thread.sleep(10000);
        }

        List<SpeechRecognitionResult> results = response.get().getResultsList();
        SpeechRecognitionResult finalResult = null;
        if (results.size() >= 1) {
            finalResult = results.get(0);
        }

        if (results.size() > 1) {
            for (int i = 1; i < results.size(); i++) {
                if (finalResult.getAlternativesList().get(0).getConfidence() <
                        results.get(i).getAlternativesList().get(0).getConfidence()) {
                    finalResult = results.get(i);
                }
            }
        }

        List<TransciptDTO> transcipts = new ArrayList<>();
        SpeechRecognitionAlternative alternative = finalResult.getAlternativesList().get(0);
        for (WordInfo wordInfo : alternative.getWordsList()) {
            transcipts.add(TransciptDTO.builder()
                    .word(wordInfo.getWord())
                    .startTime(wordInfo.getStartTime().getSeconds())
                    .endTime(wordInfo.getEndTime().getSeconds())
                    .vnTranslateText(translate(wordInfo.getWord(), "vi-VN"))
                    .build());
        }
        System.out.printf("Transcription: %s%n", alternative.getTranscript());

        return transcipts;
    }


    public String upload(MultipartFile video) throws IOException {
        BlobId blobId = BlobId.of(googleCloudEnv.getBucketName(), video.getOriginalFilename());
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        String url = "gs://" + googleCloudEnv.getBucketName() + "/" + video.getOriginalFilename();
        if (storage.get(blobId) != null) {
            return url;
        }
        storage.create(blobInfo, video.getBytes());
        return url;
    }

    @Override
    public String translate(String text, String targetLang) {
        LocationName parent = LocationName.of(googleCloudEnv.getProjectId(), "global");

        TranslateTextRequest request =
                TranslateTextRequest.newBuilder()
                        .setParent(parent.toString())
                        .setMimeType("text/plain")
                        .setTargetLanguageCode(targetLang)
                        .addContents(text)
                        .build();

        TranslateTextResponse response = translationServiceClient.translateText(request);

        return response.getTranslationsList().get(0).getTranslatedText();
    }


}
