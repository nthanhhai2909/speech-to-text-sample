package com.geekup.speechtotext.service;

import com.geekup.speechtotext.dto.TransciptDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface SpeechToTextService {

    List<TransciptDTO> transcript(MultipartFile multipartFile) throws IOException, ExecutionException, InterruptedException;

    String upload(MultipartFile multipartFile) throws IOException;

    String translate(String text, String targetLang);
}
