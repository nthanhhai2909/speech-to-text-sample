package com.geekup.speechtotext.controller;

import com.geekup.speechtotext.dto.TransciptDTO;
import com.geekup.speechtotext.service.SpeechToTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/transcript")
public class SpeechToTextController {

    @Autowired
    private SpeechToTextService speechToTextService;


    @PostMapping
    public List<TransciptDTO> transcipt(@RequestParam MultipartFile video) throws IOException, ExecutionException, InterruptedException {
        return speechToTextService.transcript(video);
    }
}
