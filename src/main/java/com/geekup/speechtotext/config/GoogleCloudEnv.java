package com.geekup.speechtotext.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "google.cloud")
@Getter
@Setter
public class GoogleCloudEnv {

    private String keyPath;

    private String projectId;

    private String bucketName;
}
