package com.geekup.speechtotext.config;


import com.google.api.gax.core.CredentialsProvider;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.speech.v1.SpeechClient;
import com.google.cloud.speech.v1.SpeechSettings;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.cloud.translate.v3.TranslationServiceClient;
import com.google.cloud.translate.v3.TranslationServiceSettings;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;

@Configuration
public class SpeechToTextAPIConfig {


    @Autowired
    private GoogleCloudEnv googleCloudEnv;

    @Bean
    public GoogleCredentials googleCredentials() throws IOException {

        return GoogleCredentials.fromStream(new FileInputStream(googleCloudEnv.getKeyPath()))
                .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
    }

    @Bean
    public SpeechClient speechClient() throws IOException {
        return SpeechClient.create(SpeechSettings.newBuilder()
                .setCredentialsProvider(() -> googleCredentials()).build());
    }

    @Bean
    public Storage storage() throws IOException {
        return StorageOptions.newBuilder()
                .setProjectId(googleCloudEnv.getProjectId())
                .setCredentials(googleCredentials()).build().getService();
    }

    @Bean
    public TranslationServiceClient translationServiceClient() throws IOException {
        return TranslationServiceClient.create(TranslationServiceSettings.newBuilder()
                .setCredentialsProvider(() -> googleCredentials())
                .build());
    }
}
